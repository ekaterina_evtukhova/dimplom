var express = require('express')
  , app = express()
  , pages = require(__dirname + '/../controllers/pages')   
  , bodyParser = require("body-parser");
  
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
  
app.use(express.static(__dirname + '/../view'))  

// configuration settings 
var engine = require('ejs-locals');
app.engine('ejs', engine);

// before this line
app.set('view engine', 'ejs');
app.set('views', __dirname + '\\..\\view\\html')

// mount routes
app.get('/', function (req, res) { res.redirect('/main') })
app.get('/main', pages.home)
app.post('/like', pages.like)

module.exports = app