var numberOfElements = 0;

var anEndpointSource = {
		endpoint: [ 'Rectangle', { width:6 , height:6 }],
		isSource: true,
		isTarget: false,
		maxConnections: 1,

		anchor: [1, 0, 1, 0]
	};

	var anEndpointDestination = {
		endpoint: [ 'Dot', { radius:3 }],
		isSource: false,
		isTarget: true,
		maxConnections: 1,
	 
		anchor: [0, 1, -1, 0]
	};
	
jsPlumb.ready(function () {
	//all windows are draggable
	jsPlumb.draggable($(".node"), {containment:"parent"});
	
	jsPlumb.importDefaults({
	  PaintStyle : {
		lineWidth:4,
		strokeStyle: "SkyBlue"
	  },
	  Connector : "Flowchart",
	  EndpointStyles : [{ fillStyle:"SkyBlue" , outlineColor:"gray" }]
	});
	
	jsPlumb.Defaults.Overlays = [
		[ "Arrow", { 
			location:1,
			id:"arrow",
			length:14,
			foldback:0.8
		} ]
	];	
	
	jsPlumb.Defaults.Endpoints = [[ 'Rectangle', { width:6 , height:6 }],
	[ 'Dot', { radius:3 }]];
	
	jsPlumb.bind("connection", function(info) {
		if (info.sourceId.indexOf("position") > -1 && info.targetId.indexOf("position") > -1) {
			alert("Error - unable to connect one type's nodes!");
			jsPlumb.detach(info.connection);
		}
		if (info.sourceId.indexOf("transition") > -1 && info.targetId.indexOf("transition") > -1) {
			alert("Error - unable to connect one type's nodes!");
			jsPlumb.detach(info.connection);
		}
	});
	
	//Add additional anchor
	$(".button_add").live("click", function () {

		var parentnode = $(this)[0].parentNode.parentNode;
		
		jsPlumb.addEndpoint(
			parentnode,
			anEndpointSource
		);
		
		jsPlumb.addEndpoint(
			parentnode,
			anEndpointDestination
		);
		
		fixEndpoints(parentnode);
	});
	
	//Remove anchor 
	$(".button_remove").live("click", function () {
		/*
		var parentnode = $(this)[0].parentNode.parentNode;

		//get list of current endpoints
		var endpoints = jsPlumb.getEndpoints(parentnode);
		
		//remove 2 last one		
		if (endpoints.length > 1) {
			jsPlumb.deleteEndpoint(endpoints[endpoints.length - 2]);
		}
		
		if (endpoints.length > 0) {
			jsPlumb.deleteEndpoint(endpoints[endpoints.length - 1]);
		}

		fixEndpoints(parentnode);
		*/
		jsPlumb.removeAllEndpoints($(this)[0].parentNode.parentNode);
		jsPlumb.detachAllConnections($(this)[0].parentNode.parentNode);
		$(this)[0].parentNode.parentNode.remove();
		
	});
	
	$(".button_add_position").click(function () {
		addPosition();		
	});
	
	$(".button_add_transition").click(function () {
		addTransition();
	});
	
	$('#saver').click(function(){
		saveFlowchart();
	});
	
	$('#loader').click(function(){
		loadFlowchart();
	});
	
});

function addPosition() {
	numberOfElements++;
	var id = "position_" + $(".position").length;
	$('<div class="position" data-nodetype="position" id="' + id + '" >').appendTo($(("#demo-canvas"))).html($(("#positionTemplate"))[0].innerHTML);
	jsPlumb.draggable($('#' + id), {containment:"parent"});
}

function addPositionWithId(id) {
	numberOfElements++;
	$('<div class="position" data-nodetype="position" id="' + id + '" >').appendTo($(("#demo-canvas"))).html($(("#positionTemplate"))[0].innerHTML);
	jsPlumb.draggable($('#' + id), {containment:"parent"});
	return id;
}

function addTransition() {
	numberOfElements++;
	var id = "transition_" + $(".transition").length;
	$('<div class="transition" data-nodetype="transition" id="' + id + '" >').appendTo($(("#demo-canvas"))).html($(("#transitionTemplate"))[0].innerHTML);
	jsPlumb.draggable($('#' + id), {containment:"parent"});
}	

function addTransitionWithId(id) {
	numberOfElements++;
	$('<div class="transition" data-nodetype="transition" id="' + id + '" >').appendTo($(("#demo-canvas"))).html($(("#transitionTemplate"))[0].innerHTML);
	jsPlumb.draggable($('#' + id), {containment:"parent"});
	return id;
}
	
//Fixes endpoints for specified target
function fixEndpoints(parentnode) {
	
	//get list of current endpoints
	var endpoints = jsPlumb.getEndpoints(parentnode);

	//there are 2 types - input and output
	
	var inputAr = $.grep(endpoints, function (elementOfArray, indexInArray) {
		return elementOfArray.isSource; //input
	});
	
	var outputAr = $.grep(endpoints, function (elementOfArray, indexInArray) {
		return elementOfArray.isTarget; //output
	});

	calculateEndpoint(inputAr, true);
	calculateEndpoint(outputAr, false);

	jsPlumb.repaintEverything();
};

//recalculate endpoint anchor position manually
function calculateEndpoint(endpointArray, isInput) {

	//multiplyer
	var mult = 1 / (endpointArray.length+1);
	for (var i = 0; i < endpointArray.length; i++) {			
		if (isInput) {
			//position
			endpointArray[i].anchor.x = 1;
			endpointArray[i].anchor.y = mult * (i + 1);
		} 
		else {
			//position
			endpointArray[i].anchor.x = 0;
			endpointArray[i].anchor.y = mult * (i + 1);
		}
	}
};

function saveFlowchart(){
	//alert("Hello");
	var positions = []
	
	$(".position").each(function (idx, elem) {
		var $elem = $(elem);
		var endpoints = jsPlumb.getEndpoints($elem.attr('id'));
		console.log('endpoints of '+$elem.attr('id'));
		console.log(endpoints);
		if ($elem.attr('id') != 'positionTemplate') {
			positions.push({
				blockId: $elem.attr('id'),
				nodetype: $elem.attr('data-nodetype'),
				//chips: parseInt($elem.find(".button_container_position .input_field input").val()),
				chips: 0,
				positionX: parseInt($elem.css("left"), 10),
				positionY: parseInt($elem.css("top"), 10)
			});
		}
	});
	
	var transitions = []
	
	$(".transition").each(function (idx, elem) {
		var $elem = $(elem);
		var endpoints = jsPlumb.getEndpoints($elem.attr('id'));
		console.log('endpoints of '+$elem.attr('id'));
		console.log(endpoints);
		if ($elem.attr('id') != 'transitionTemplate') {
			transitions.push({
				blockId: $elem.attr('id'),
				nodetype: $elem.attr('data-nodetype'),
				positionX: parseInt($elem.css("left"), 10),
				positionY: parseInt($elem.css("top"), 10),
				actionType: $elem.find(".button_container_transition .input_field_tr input").val()
			});
		}
	});
	
	var connections = [];
	$.each(jsPlumb.getConnections(), function (idx, connection) {
      connections.push({
      connectionId: connection.id,
      pageSourceId: connection.sourceId,
      pageTargetId: connection.targetId,
      anchors: $.map(connection.endpoints, function(endpoint) {
        return [[endpoint.anchor.x, 
        endpoint.anchor.y]];
      })
    });
  });
	
	var flowChart = {};
	//flowChart.nodes = nodes;
	flowChart.positions = positions;
	flowChart.transitions = transitions;
	flowChart.connections = connections;
	//flowChart.numberOfElements = numberOfElements;
	
	var flowChartJson = JSON.stringify(flowChart);
	//console.log(flowChartJson);
	
	$('#jsonOutput').val(flowChartJson);
}

function loadFlowchart(){
	console.log('start');
	var flowChartJson = $('#jsonOutput').val();
	var flowChart = JSON.parse(flowChartJson);
	
	var positions = flowChart.positions;
	$.each(positions, function( index, elem ) {
		console.log('positions'+elem.nodetype);
		if(elem.nodetype === 'startpoint'){
			repositionElement('startpoint', elem.positionX, elem.positionY);
		}else if(elem.nodetype === 'endpoint'){
			repositionElement('endpoint', elem.positionX, elem.positionY);
		}else if(elem.nodetype === 'position'){
			var id = addPositionWithId(elem.blockId);
			repositionElement(id, elem.positionX, elem.positionY);
		}else if(elem.nodetype === 'transition'){
			var id = addTransitionWithId(elem.blockId);
			repositionElement(id, elem.positionX, elem.positionY);
		}else{
			
		}
	});
	
	var transitions = flowChart.transitions;
	$.each(transitions, function( index, elem ) {
		if(elem.nodetype === 'startpoint'){
			repositionElement('startpoint', elem.positionX, elem.positionY);
		}else if(elem.nodetype === 'endpoint'){
			repositionElement('endpoint', elem.positionX, elem.positionY);
		}else if(elem.nodetype === 'position'){
			var id = addPositionWithId(elem.blockId);
			repositionElement(id, elem.positionX, elem.positionY);
		}else if(elem.nodetype === 'transition'){
			var id = addTransitionWithId(elem.blockId);
			repositionElement(id, elem.positionX, elem.positionY);
		}else{
			
		}
	});
	
	var connections = flowChart.connections;
	$.each(connections, function( index, elem ) {
		console.log('hi');

		var sPoint = jsPlumb.addEndpoint(
			elem.pageSourceId,
			anEndpointSource
		);
		
		var ePoint = jsPlumb.addEndpoint(
			elem.pageTargetId,
			anEndpointDestination
		);
		
		fixEndpoints(elem.pageSourceId);
		fixEndpoints(elem.pageTargetId);
		
		 var connection1 = jsPlumb.connect({
			source: elem.pageSourceId,
			target: elem.pageTargetId,
			anchors: elem.anchors
			
		});
		
	});
	
	numberOfElements = flowChart.numberOfElements;						
	
}
function repositionElement(id, posX, posY){
	$('#'+id).css('left', posX);
	$('#'+id).css('top', posY);
	//concole.log('hi');
	jsPlumb.repaint(id);
}
