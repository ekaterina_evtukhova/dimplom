﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetriNetwork;

namespace ApplicationPetri
{
    public class Point : Entity
    {
        private static Random r = new Random();
        private Int32 x;
        private Int32 y;
        public enum CAR_COLOR { BLUE, RED, YELLOW, UNKNOWN };

        private CAR_COLOR color;

        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
            color = (CAR_COLOR)(r.Next(3));
        }

        public CAR_COLOR getColor()
        {
            return color;
        }

        public override void setX(int x)
        {
            this.x = x;
        }

        public override void setY(int y)
        {
            this.y = y;
        }

        public override int getX()
        {
            return x;
        }

        public override int getY()
        {
            return y;
        }

        public void updatePoint(Point p)
        {
            this.x += p.x;
            this.y += p.y;
        }

        public bool Equals(Point p)
        {
            if ((object)p == null)
            {
                return false;
            }

            return (x == p.x) && (y == p.y);
        }

    }
}
