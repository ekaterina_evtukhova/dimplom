﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationPetri
{
    public class Semaphore : Entity
    {
        private Int32 x;
        private Int32 y;

        public enum SEMAPHORE_COLOR { RED, GREEN };

        public Semaphore(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public override void setX(int x)
        {
            this.x = x;
        }

        public override void setY(int y)
        {
            this.y = y;
        }

        public override int getX()
        {
            return x;
        }

        public override int getY()
        {
            return y;
        }

        public bool Equals(Semaphore p)
        {
            if ((object)p == null)
            {
                return false;
            }
            return (x == p.x) && (y == p.y);
        }
    }
}
