﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApplicationPetri;

namespace PetriNetwork
{
    abstract class ActionType
    {
        public abstract void work(ref Point p);
    }
}
