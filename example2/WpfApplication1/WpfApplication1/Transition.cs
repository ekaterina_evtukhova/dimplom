﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApplicationPetri;

namespace PetriNetwork
{
    public class Transition : Node
    {
        public string blockId { get; set; }
        public string nodetype { get; set; }
        public int positionX { get; set; }
        public int positionY { get; set; }
        public string actionType { get; set; }

        private ActionType actionTypeNode;
        
        public void work(ref Point point)
        {
            actionTypeNode = ActionTypeCreator.generate(actionType);
            if (actionTypeNode != null)
                actionTypeNode.work(ref point);
        }        
    }
}
