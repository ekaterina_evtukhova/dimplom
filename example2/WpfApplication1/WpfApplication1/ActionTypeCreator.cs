﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetriNetwork
{
    class ActionTypeCreator
    {
        public static ActionType generate(string actionType)
        {
            switch (actionType)
            {
                case NetworkRules.UP:
                    return new ActionTypeForward();
                case NetworkRules.STOP:
                    return new ActionTypeStop();
                case NetworkRules.RIGHT:
                    return new ActionTypeRight();
                case NetworkRules.LEFT:
                    return new ActionTypeLeft();
                default:
                    return null;
            }
        }
    }
}
