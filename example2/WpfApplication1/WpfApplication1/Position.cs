﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetriNetwork
{
    public class Position : Node
    {
        public string blockId { get; set; }
        public string nodetype { get; set; }
        public int chips { get; set; }
        public int positionX { get; set; }
        public int positionY { get; set; }

        private List<Chip> listChips;

        public void addChip(Chip chip) 
        {
            if (listChips == null)
                listChips = new List<Chip>();
            listChips.Add(chip);
            chips++;
        }

        public void deleteChip(Chip chip)
        {
            listChips.Remove(chip);
        }

        public Chip isChip(String action)
        {
            foreach (Chip chip in listChips)
                if (chip != null)
                    return chip;
            return null;
        }
    }
}
