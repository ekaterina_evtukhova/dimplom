﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Threading.Tasks;

namespace ApplicationPetri
{
    public class CommonData
    {
        private static Int32 timeModeling;

        private static Int32 timeRed;
        private static Int32 timeGreen;

        private static int x;
        private static int y;

        private static int intensity;

        private static int sizeRect;

        private static ApplicationPetri.Semaphore.SEMAPHORE_COLOR prevColor;

        public void setSizeRect(int sizeRect)
        {
            CommonData.sizeRect = sizeRect;
        }

        public int getSizeRect()
        {
            return CommonData.sizeRect;
        }

        public void setIntensity(int intensity)
        {
            CommonData.intensity = intensity;
        }

        public int getInstensity()
        {
            return CommonData.intensity;
        }

        public void setSize(int x, int y)
        {
            CommonData.x = x;
            CommonData.y = y;
        }

        public int getX()
        {
            return CommonData.x;
        }

        public int getY()
        {
            return CommonData.y;
        }

        public void setTime(Int32 time)
        {
            timeModeling = time;
        }

        public void setSemaforeTime(Int32 timeRed, Int32 timeGreen)
        {
            CommonData.timeRed = timeRed;
            CommonData.timeGreen = timeGreen;
        }

        public ApplicationPetri.Semaphore.SEMAPHORE_COLOR semaforeColor(int time)
        {
            if (time % (getRedTime() + getGreenTime()) - getRedTime() < 0)
            {
                prevColor = ApplicationPetri.Semaphore.SEMAPHORE_COLOR.RED;
                return ApplicationPetri.Semaphore.SEMAPHORE_COLOR.RED;
            }
            if (prevColor == ApplicationPetri.Semaphore.SEMAPHORE_COLOR.RED)
            {
                Statistic.Instance.setDeadlockLength( Container.Instance.getDeadlockLength());
            }
            prevColor = ApplicationPetri.Semaphore.SEMAPHORE_COLOR.GREEN;
            return ApplicationPetri.Semaphore.SEMAPHORE_COLOR.GREEN;
        }

        public bool semaforeState(int time)
        {
            if (time % (getRedTime() + getGreenTime()) - getRedTime() < 0)
                return false;
            return true;
        }


        public Int32 getTime()
        {
            return timeModeling;
        }

        public Int32 getRedTime()
        {
            return timeRed;
        }

        public Int32 getGreenTime()
        {
            return timeGreen;
        }

        private static volatile CommonData instance;
        private static object syncRoot = new Object();
        private CommonData() {}

        public static CommonData Instance
        {
            get 
            {
                if (instance == null) 
                {
                lock (syncRoot) 
                {
                    if (instance == null)
                        instance = new CommonData();
                }
                }

                return instance;
            }
        }
    }
}
