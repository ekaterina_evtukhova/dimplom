﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationPetri
{
    class Statistic
    {
        private static volatile Statistic instance;
        private static object syncRoot = new Object();

        private int transactions = 0;
        private int time = 0;
        private int semafores = 0;
        private int maxDeadlockLength = 0;

        private LinkedList<Int32> transactionsList = new LinkedList<Int32>();
        private LinkedList<Int32> timeList = new LinkedList<Int32>();
        private LinkedList<Int32> semaforesList = new LinkedList<Int32>();
        private LinkedList<Int32> maxDeadlockLengthList = new LinkedList<Int32>();
        private int count = -1;

        public LinkedList<Int32> getTransactionsList()
        {
            return transactionsList;
        }

        public LinkedList<Int32> getTimeList()
        {
            return timeList;
        }

        public LinkedList<Int32> getSemaforesList()
        {
            return semaforesList;
        }

        public LinkedList<Int32> getMaxDeadlockLengthList()
        {
            return maxDeadlockLengthList;
        }

        public void clear()
        {
            if (count != -1)
            {
                transactionsList.AddLast(transactions);
                timeList.AddLast(time);
                semaforesList.AddLast(semafores);
                maxDeadlockLengthList.AddLast(maxDeadlockLength);                

                transactions = 0;
                time = 0;
                semafores = 0;
                maxDeadlockLength = 0;
            }
            else
                count++;
        }

        public void addTransaction()
        {
            transactions++;
        }

        public int getTransactions()
        {
            return transactions;
        }

        public void setDeadlockLength(int length)
        {
            if (length > maxDeadlockLength)
            {
                maxDeadlockLength = length;
            }
        }

        public int getMaxDeadlockLength()
        {
            return maxDeadlockLength;
        }

        public int getSemafores()
        {
            return Container.Instance.getSemafores().Count / CommonData.Instance.getX();
        }

        public void setTime(int time)
        {
            this.time = time;
        }

        public int getTime()
        {
            return time;
        }
        private Statistic() { }

        public static Statistic Instance
        {
            get 
            {
                if (instance == null) 
                {
                lock (syncRoot) 
                {
                    if (instance == null)
                        instance = new Statistic();
                }
                }

                return instance;
            }
        }
    }
}
