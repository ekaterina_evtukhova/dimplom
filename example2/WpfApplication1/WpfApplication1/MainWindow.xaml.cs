﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Threading;
using System.Text.RegularExpressions;
using PetriNetwork;
using Microsoft.Win32;
using ApplicationPetri;

namespace WpfApplication1
{
    public static class ExtensionMethods
    {
        private static Action EmptyDelegate = delegate() { };

        public static void Refresh(this UIElement uiElement)
        {
            uiElement.Dispatcher.Invoke(DispatcherPriority.Render, EmptyDelegate);
        }
    }

    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {       

        private const int size = 30;
        
        public MainWindow()
        {
            InitializeComponent();
            
            CommonData.Instance.setSizeRect(size);
            FormAdapter.Instance.apply(MainCanvas, textBoxWidth.Text, textBoxLength.Text);
            FormAdapter.Instance.updateBackPattern(MainCanvas, 0);
        }

        private void UpdateBackPattern(object sender, SizeChangedEventArgs e)
        {
            FormAdapter.Instance.updateBackPattern(MainCanvas, 0);
        }
                
        private void MainCanvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            
        }

        private void btnApply_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FormAdapter.Instance.apply(MainCanvas, textBoxWidth.Text, textBoxLength.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Неправильные размеры дорожного полотна!");
            }           
            
        }

        private void openMenuItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
            FormAdapter.Instance.openFile(new OpenFileDialog());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Неверный формат входного файла!");
            }  
        }

        private void aboutMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("ПО к бакалаврской работе\n" +
                "Выполнила: Евтухова Е.С.\n" +
                "Научный руководитель: Рудаков И.В.");
        }

        private void exitMenuItem_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            Environment.Exit(0);
        }

        private void statisticMenuItem_Click(object sender, RoutedEventArgs e)
        {
            DateTime dt = FormAdapter.Instance.saveStatistic();

            MessageBox.Show("Статистика сохранена в папку " +
                System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
                "\nДата = " + dt.ToString());
            /*
            MessageBox.Show("\tСтатистика:\n" +
                "\nКоличество сгенерированных транзактов: " + Statistic.Instance.getTransactions() +
                "\nВремя выполнения (сек): " + Statistic.Instance.getTime() +
                "\nМаксимальная длина пробки: " + Statistic.Instance.getMaxDeadlockLength());
             * */
        } 
        

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            Statistic.Instance.clear();
            Container.Instance.setPoints(new List<ApplicationPetri.Point>());
            if (FormAdapter.Instance.getRootObject() != null)
            {
                try
                {
                    if (Convert.ToInt32(textBoxIntensity.Text) <= 0 ||
                        Convert.ToInt32(textBoxIntensity.Text) > 100)
                        throw new Exception();

                    FormAdapter.Instance.start(textBoxSemaforeTime.Text,
                        textBoxTimeModeling.Text, textBoxIntensity.Text);

                    textBoxWidth.IsEnabled = false;
                    textBoxLength.IsEnabled = false;
                    btnApply.IsEnabled = false;

                    FormAdapter.Instance.newThread(MainCanvas);

                    textBoxWidth.IsEnabled = true;
                    textBoxLength.IsEnabled = true;
                    btnApply.IsEnabled = true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Неправильные начальные данные!");
                }
            }
            else
            {
                MessageBox.Show("Сеть Петри не загружена!");
            }
        }

        private void MainCanvas_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            int x = (Convert.ToInt32(e.GetPosition(MainCanvas).X) / size);
            int y = (Convert.ToInt32(e.GetPosition(MainCanvas).Y) / size);
            for (int i = 0; i < CommonData.Instance.getX(); i++)
            {
                if (Container.Instance.addSemafore(i, y))
                    FormAdapter.Instance.drawRectSemafore(MainCanvas, i, y,
                        ApplicationPetri.Semaphore.SEMAPHORE_COLOR.RED);
            }
        }

        private void MainCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            /*
            int x = (Convert.ToInt32(e.GetPosition(MainCanvas).X) / size);
            int y = (Convert.ToInt32(e.GetPosition(MainCanvas).Y) / size);
            
            ApplicationPetri.Point.CAR_COLOR color = Container.Instance.addObject(x, y);
            if (color != ApplicationPetri.Point.CAR_COLOR.UNKNOWN)
                FormAdapter.Instance.drawRect(MainCanvas, x, y, color);
             * */
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            FormAdapter.Instance.clear(MainCanvas);
        }
        
    }
}
