﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using PetriNetwork;

namespace ApplicationPetri
{
    class Container
    {

        private List<Point> points = new List<Point>();
        private List<Semaphore> semafores = new List<Semaphore>();

        private static volatile Container instance;
        private static object syncRoot = new Object();

        public int getDeadlockLength()
        {
            int max = 0;
            var groupsSemafores =
                    from semafore in semafores
                    group semafore by semafore.getY();
            foreach (var ySemafores in  groupsSemafores)
            {
                int y = ySemafores.Key-1;

                int[] roadLocks = new int[CommonData.Instance.getX()];
                for (int i=0; i < roadLocks.Length; i++) {
                    roadLocks[i] = 0;
                }

                bool flag = true;

                while (flag)
                {
                    flag = false;

                    List<Point> group = points.FindAll(x => x.getY() == y);

                    foreach (Point point in group)
                    {
                        flag = true;
                        roadLocks[point.getX()]++;
                    }

                    for (int i = 0; i < roadLocks.Length; i++)
                    {
                        if (roadLocks[i] > max)
                            max = roadLocks[i];
                    }

                    //Statistic.Instance.setDeadlockLength(max);
                    y--;
                }
            }
            return max;
        }

        
        private Container() {}

        public static Container Instance
        {
            get 
            {
                if (instance == null) 
                {
                lock (syncRoot) 
                {
                    if (instance == null)
                        instance = new Container();
                }
                }

                return instance;
            }
        }

        public void setPoints(List<Point> pointsList) 
        {
            points = null;
            points = pointsList;
        }

        public List<Point> getPoints()
        {
            return points;
        }

        public List<Semaphore> getSemafores()
        {
            return semafores;
        }

        public void run(int time)
        {            
            for (int i = 0; i < points.Count; i++)
            {                    
                Point p = (Point)points[i];
                MainPetriAlgorithm mainAlgo = new MainPetriAlgorithm(ref p);
                mainAlgo.run(time);
                points[i] = p;                    
            }                
        }
        
        public ApplicationPetri.Point.CAR_COLOR addObject(int x, int y)
        {
            foreach (Point p in points)
            {
                if (p.getX() == x && p.getY() == y)
                    return ApplicationPetri.Point.CAR_COLOR.UNKNOWN;
            }
            Point point = new Point(x, y);
            points.Add(point);
            Statistic.Instance.addTransaction();
            return point.getColor();
        }

        public bool addSemafore(int x, int y)
        {
            foreach (Semaphore s in semafores)
            {
                if (s.getX() == x && s.getY() == y)
                    return false;
            }
            semafores.Add(new Semaphore(x, y));
            return true;            
        }
        
        public void clearObjects()
        {
            semafores.Clear();
            points.Clear();
            semafores = new List<Semaphore>();
            points = new List<Point>();
        }
    }
}
