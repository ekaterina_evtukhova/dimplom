﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApplicationPetri;

namespace PetriNetwork
{
    class ActionTypeLeft : ActionType
    {
        public override void work(ref Point point)
        {
            point.updatePoint(new Point(1, 1));
        }
    }
}
