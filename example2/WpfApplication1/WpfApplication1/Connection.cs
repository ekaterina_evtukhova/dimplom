﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetriNetwork
{
    public class Connection
    {
        public string connectionId { get; set; }
        public string pageSourceId { get; set; }
        public string pageTargetId { get; set; }
        public List<List<double>> anchors { get; set; }
    }
}
