﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationPetri
{
    public abstract class Entity
    {
        public abstract void setX(int x);
        public abstract void setY(int y);
        public abstract int getX();
        public abstract int getY();
    }
}
