﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApplicationPetri;

namespace PetriNetwork
{
    public class MainPetriAlgorithm
    {
        private RootObject rootObject;
        private Int32 x;
        private Int32 y;
        private Point point;

        public MainPetriAlgorithm()
        {
            rootObject = FormAdapter.Instance.getRootObject();
        }

        public MainPetriAlgorithm(ref Point point)
        {
            this.point = point;
            rootObject = FormAdapter.Instance.getRootObject();
        }

        public void setPoint(ref Int32 x, ref Int32 y)
        {
            this.x = x;
            this.y = y;
        }

        public void setRootObject(RootObject rootObject)
        {
            this.rootObject = rootObject;
        }

        public void updateObject(Point point)
        {
            this.x = this.x + point.getX();
            this.y = this.y + point.getY();
        }

        public void run(int time)
        {
            bool isChange = true;

            //while petri network working
            while (isChange)
            {
                Transition t = rootObject.getStartTransition();                
                Chip chip = new Chip();
                chip.setChip(point, time);

                List<String> transitionConnectionsFrom =
                                rootObject.getConnectionsByPageSourceId(t.blockId);
                foreach (String from in transitionConnectionsFrom)
                {
                    Connection con = rootObject.getConnectionByName(from);
                    Position pos = rootObject.getPositionByName(con.pageTargetId);
                    pos.addChip(chip);
                }


                foreach (Transition transition in rootObject.transitions)
                {
                    isChange = false;
                    bool transitionWork = true;

                    List<String> transitionConnections =
                        rootObject.getConnectionsByPageTargetId(transition.blockId);

                    IEnumerable<IGrouping<String, String>> groupByPosition =
                        rootObject.groupBy(transitionConnections);

                    foreach (IGrouping<String, String> positionMoves in
                        groupByPosition)
                    {
                        if (rootObject.getConnectionsBySourceAndTargetId(positionMoves.Key,
                            transition.blockId).Count >
                            rootObject.getPositionByName(positionMoves.Key).chips)
                        {
                            transitionWork = false;
                            break; 
                        }
                    }


                    if (transitionWork)
                    {
                        foreach (IGrouping<String, String> positionMoves in
                            groupByPosition)
                        {
                            if (!NetworkRules.ruleTransitionUnableWork(rootObject,
                                transition, positionMoves))
                            {

                                if (!(NetworkRules.ruleCanUp(rootObject, transition, positionMoves)) &&
                                    rootObject.getPositionByName(positionMoves.Key).isChip(transition.actionType) != null)
                                {
                                    transition.work(ref point);                                    
                                }
                                {
                                    isChange = true;
                                    rootObject.getPositionByName(positionMoves.Key).chips -=
                                    rootObject.getConnectionsBySourceAndTargetId(positionMoves.Key,
                                    transition.blockId).Count;
                                    Chip c = rootObject.getPositionByName(positionMoves.Key).isChip(transition.actionType);
                                    rootObject.getPositionByName(positionMoves.Key).deleteChip(c);

                                    List<String> transitionConnectionsFrom2 =
                                        rootObject.getConnectionsByPageSourceId(transition.blockId);
                                    foreach (String from in transitionConnectionsFrom2)
                                    {
                                        Connection con = rootObject.getConnectionByName(from);
                                        Position pos = rootObject.getPositionByName(con.pageTargetId);
                                        pos.addChip(c);
                                    }
                                }
                            }

                        }

                    }

                }
            }
        }

    }
}
