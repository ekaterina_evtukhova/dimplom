﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetriNetwork
{
    class NetworkRules
    {
        public const String FORWARD = "f";
        public const String MOVEMENT = "m";
        public const String RIGHT = "r";
        public const String LEFT = "l";
        public const String UP = "u";
        public const String STOP = "s";

        private static bool ruleCanForward(RootObject rootObject, 
            Transition transition, IGrouping<String, String> positionMoves)
        {
            return (transition.actionType.Equals(FORWARD) &&
                rootObject.getPositionByName(positionMoves.Key).isChip(transition.actionType).type
                != Chip.CHIP_TYPE.FORWARD);
        }

        private static bool ruleCanMovement(RootObject rootObject, 
            Transition transition, IGrouping<String, String> positionMoves)
        {
            return (transition.actionType.Equals(MOVEMENT) &&
            rootObject.getPositionByName(positionMoves.Key).isChip(transition.actionType).type
            != Chip.CHIP_TYPE.MOVEMENT);
        }

        private static bool ruleCanRight(RootObject rootObject,
            Transition transition, IGrouping<String, String> positionMoves)
        {
            return (transition.actionType.Equals(RIGHT) &&
            rootObject.getPositionByName(positionMoves.Key).isChip(transition.actionType).chipTrue
            != Chip.CHIP_STATE.MOVE);
        }

        private static bool ruleCanLeft(RootObject rootObject,
            Transition transition, IGrouping<String, String> positionMoves)
        {
            return (transition.actionType.Equals(LEFT) &&
            rootObject.getPositionByName(positionMoves.Key).isChip(transition.actionType).chipTrue
            != Chip.CHIP_STATE.LEFT);
        }

        private static bool ruleCanStop(RootObject rootObject,
            Transition transition, IGrouping<String, String> positionMoves)
        {
            return (transition.actionType.Equals(STOP) &&
            rootObject.getPositionByName(positionMoves.Key).isChip(transition.actionType).chipTrue
            != Chip.CHIP_STATE.UNABLE);
        }

        public static bool ruleCanUp(RootObject rootObject,
            Transition transition, IGrouping<String, String> positionMoves)
        {
            return transition.actionType.Equals(UP) &&
            rootObject.getPositionByName(positionMoves.Key).isChip(transition.actionType).chipTrue
             == Chip.CHIP_STATE.UNABLE;
        }

        public static bool ruleTransitionUnableWork(RootObject rootObject, 
            Transition transition, IGrouping<String, String> positionMoves)
        {
            return (ruleCanForward(rootObject, transition, positionMoves) ||
            ruleCanMovement(rootObject, transition, positionMoves) ||
            ruleCanRight(rootObject, transition, positionMoves) ||
            ruleCanLeft(rootObject, transition, positionMoves));              
        }
    }
}
