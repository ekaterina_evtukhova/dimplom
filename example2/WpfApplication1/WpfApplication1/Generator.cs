﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationPetri
{
    class Generator
    {
        private Random rnd;
        private double lambda;
        private int roadId;

        public Generator(int roadId, double lambda)
        {
            lambda = lambda / 100.0;
            rnd = new Random();
            this.roadId = roadId;

            if (roadId == (CommonData.Instance.getX() - 1))
            {
                //this.lambda = (0.168 * lambda);
                this.lambda = (0.160 * lambda);
                //if (this.lambda > 42)
                    //this.lambda -= 42;
            }
            else if (roadId == 0)
            {
                //this.lambda = (0.4 * lambda + 50);
                this.lambda = (0.4 * lambda);
            }
            else
            {
                //this.lambda = ((0.441 * lambda - 18) / (CommonData.Instance.getX()-2));
                this.lambda = ((0.440 * lambda) / (double)(CommonData.Instance.getX() - 2));
            }

            if (this.lambda == 0)
            {
                this.lambda = 0.001;
            }

        }

        public int generate()
        {
            double tmp = rnd.Next(100) / 100.0;
            double res = Math.Log(1-tmp) /(lambda);
            if (res < 0)
                res *= -1;
            return (int)res;

        }
    }
}
