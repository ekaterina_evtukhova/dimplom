﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetriNetwork
{
    public class RootObject
    {
        public List<Position> positions { get; set; }
        public List<Transition> transitions { get; set; }
        public List<Connection> connections { get; set; }

        public Transition getStartTransition()
        {
            foreach (Transition t in transitions)
            {
                if (connections.Find(x => x.pageTargetId.Equals(t.blockId)) == null)
                    if (connections.Find(x => x.pageSourceId.Equals(t.blockId)) != null)
                        return t;
            }
            return null;
        }

        public int countOfGroup(IGrouping<String, String> group, String key)
        {
            int result = 0;
            foreach (String name in group)
            {
                if (name.Equals(key))
                    result++;
            }
            return result;
        }

        public IEnumerable<IGrouping<String, String>> groupBy(List<String> connections)
        {
            return connections.GroupBy(x => getConnectionByName(x).pageSourceId,
                x => getConnectionByName(x).pageTargetId);
        }

        public List<String> getConnectionsByPageSourceId(String nodeName)
        {
            IEnumerable<String> result = new List<String>();
            result = 
                from con in connections
                where con.pageSourceId.Equals(nodeName)
                select con.connectionId;

            return new List<String>(result);
        }

        public List<String> getConnectionsByPageTargetId(String nodeName)
        {
            IEnumerable<String> result = new List<String>();
            result = 
                from con in connections
                where con.pageTargetId.Equals(nodeName)
                select con.connectionId;

            return new List<String>(result);
        }

        public List<String> getConnectionsBySourceAndTargetId(String nodeNameSource, 
            String nodeNameTarget)
        {
            IEnumerable<String> result = new List<String>();
            result = 
                from con in connections
                where con.pageTargetId.Equals(nodeNameTarget)
                    & con.pageSourceId.Equals(nodeNameSource)
                select con.connectionId;

            return new List<String>(result);
        }

        public Connection getConnectionByName(String name)
        {
            return connections.Find(x => x.connectionId.Equals(name));
        }

        public Position getPositionByName(String name)
        {
            return positions.Find(x => x.blockId.Equals(name));
        }

        public Transition getTransitionByName(String name)
        {
            return transitions.Find(x => x.blockId.Equals(name));
        }
    }
}
