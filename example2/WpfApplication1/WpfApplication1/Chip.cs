﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApplicationPetri;

namespace PetriNetwork
{
    public class Chip
    {
        public enum CHIP_TYPE { FORWARD, MOVEMENT };
        public enum CHIP_STATE { UNABLE, MOVE, LEFT };
        public CHIP_TYPE type;
        public CHIP_STATE chipTrue;

        public void setChip(Point p, int time)
        {
            Point pointNext = new Point(p.getX(), p.getY() + 1);
            List<Point> points = ApplicationPetri.Container.Instance.getPoints();
            bool result = true;
            foreach (Point point in points)
            {
                if (pointNext.Equals(point))
                {
                    result = false;
                    break;
                }
            }
            List<Semaphore> semafores = ApplicationPetri.Container.Instance.getSemafores();
            foreach (Semaphore s in semafores)
            {
                if (pointNext.getX() == s.getX() && pointNext.getY() == s.getY()
                    && !CommonData.Instance.semaforeState(time))
                {
                    result = false;
                    break;
                }
            }

            if (result == true)
                setUp(p, time);
            else
                setMovement(p, time);            
        }

        public void setMovement(Point point, int time)
        {
            type = CHIP_TYPE.MOVEMENT;
            List<Point> points = ApplicationPetri.Container.Instance.getPoints();
            Point pointRight = new Point(point.getX() - 1, point.getY() + 1);
            Point pointLeft = new Point(point.getX() + 1, point.getY() + 1);
            CHIP_STATE result = CHIP_STATE.MOVE;

            bool left = pointLeft.getX() < CommonData.Instance.getX() ? true : false;
            bool right = pointRight.getX() > -1 ? true : false;

            foreach (Point p in points)
            {
                if (pointRight.Equals(p))
                    right = false;
                if (pointLeft.Equals(p))
                    left = false;
            }

            List<ApplicationPetri.Semaphore> semafores = ApplicationPetri.Container.Instance.getSemafores();
            foreach (ApplicationPetri.Semaphore s in semafores)
            {
                if (pointRight.getX() == s.getX() && pointRight.getY() == s.getY()
                    && !CommonData.Instance.semaforeState(time))
                    right = false;
                if (pointLeft.getX() == s.getX() && pointLeft.getY() == s.getY()
                   && !CommonData.Instance.semaforeState(time))
                    left = false;
            }

            if (right)
                chipTrue = CHIP_STATE.MOVE;
            else if (left)
                chipTrue = CHIP_STATE.LEFT;
            else
            {
                chipTrue = CHIP_STATE.UNABLE;
                type = CHIP_TYPE.FORWARD;
            }
        }

        public void setUp(Point point, int time)
        {
            type = CHIP_TYPE.FORWARD;
            List<Point> points = ApplicationPetri.Container.Instance.getPoints();
            Point pointNext = new Point(point.getX(), point.getY() + 1);
            //Point pointNext2 = new Point(point.getX(), point.getY() + 2);
            CHIP_STATE result = CHIP_STATE.MOVE;

            foreach (Point p in points)
            {
                if (pointNext.Equals(p) 
                    //|| pointNext2.Equals(p)
                    )
                    result = CHIP_STATE.UNABLE;
            }

            List<ApplicationPetri.Semaphore> semafores = ApplicationPetri.Container.Instance.getSemafores();
            foreach (ApplicationPetri.Semaphore s in semafores)
            {
                if (pointNext.getX() == s.getX() && pointNext.getY() == s.getY()
                    && !CommonData.Instance.semaforeState(time))
                    result = CHIP_STATE.UNABLE;
            }

            chipTrue = result;
        }

        public bool Equals(Chip c)
        {
            // If parameter is null return false:
            if ((object)c == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (type == c.type);
        }

    }
}
