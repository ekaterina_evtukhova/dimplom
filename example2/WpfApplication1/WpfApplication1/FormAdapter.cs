﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using System.Text.RegularExpressions;
using System.Windows.Threading;
using System.Threading;
using Microsoft.Win32;
using PetriNetwork;
using System.Diagnostics;
using WpfApplication1;
using System.Text;
using System.IO;

namespace ApplicationPetri
{
    class FormAdapter
    {
        private static volatile FormAdapter instance;
        private static object syncRoot = new Object();
        private static RootObject rootObject;
        private FormAdapter() { }

        public static FormAdapter Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new FormAdapter();
                    }
                }

                return instance;
            }
        }

        public RootObject getRootObject()
        {
            return rootObject;
        }

        public void openFile(OpenFileDialog openFileDialog) {
            Nullable<bool> resultOpen = openFileDialog.ShowDialog();
            if (resultOpen == true)
            {
                System.IO.StreamReader sr = new 
                    System.IO.StreamReader(openFileDialog.FileName);

                rootObject = 
                    Serializer.Deserialize<RootObject>(openFileDialog.FileName);
                sr.Close();
            }
        }

        public void apply(Canvas canvas, String sizeX, String sizeY)
        {
            int size = CommonData.Instance.getSizeRect();
            canvas.Width = Convert.ToInt32(sizeX) * size;
            canvas.Height = Convert.ToInt32(sizeY) * size;
            CommonData.Instance.setSize(Convert.ToInt32(sizeX),
                Convert.ToInt32(sizeY));
        }

        public void start(String timeSemaphore, String timeModeling, String intensity)
        {
            CommonData.Instance.setSemaforeTime(
                Convert.ToInt32(Regex.Split(timeSemaphore, "/")[0]),
                Convert.ToInt32(Regex.Split(timeSemaphore, "/")[1]));
            CommonData.Instance.setTime(Convert.ToInt32(timeModeling));

            CommonData.Instance.setIntensity(Convert.ToInt32(intensity)* CommonData.Instance.getX());
        }

        public void drawRect(Canvas canvas, int left, int top,
            Point.CAR_COLOR color)
        {
            int size = CommonData.Instance.getSizeRect();
            Uri carUri = null;
            switch (color)
            {
                case (Point.CAR_COLOR.BLUE):
                    carUri = new Uri("pack://application:,,,/images/car.png", UriKind.Absolute);
                    break;
                case (Point.CAR_COLOR.RED):
                    carUri = new Uri("pack://application:,,,/images/car2.png", UriKind.Absolute);
                    break;
                case (Point.CAR_COLOR.YELLOW):
                    carUri = new Uri("pack://application:,,,/images/car3.png", UriKind.Absolute);
                    break;

            }
            BitmapImage carImg = new BitmapImage(carUri);

            Image carImage = new Image
            {
                Width = size,
                Height = size,
                Source = carImg
            };
            canvas.Children.Add(carImage);
            Canvas.SetTop(carImage, top * size);
            Canvas.SetLeft(carImage, left * size);
        }

        public void drawRectSemafore(Canvas canvas, int left, int top,
            ApplicationPetri.Semaphore.SEMAPHORE_COLOR color)
        {
            int size = CommonData.Instance.getSizeRect();
            Uri semaphoreUri;
            if (color == ApplicationPetri.Semaphore.SEMAPHORE_COLOR.RED)
                semaphoreUri = new Uri("pack://application:,,,/images/red.png", UriKind.Absolute);
            else
                semaphoreUri = new Uri("pack://application:,,,/images/green.png", UriKind.Absolute);
            BitmapImage semaphoreImg = new BitmapImage(semaphoreUri);

            Image semaphoreImage = new Image
            {
                Width = size,
                Height = size,
                Source = semaphoreImg
            };
            canvas.Children.Add(semaphoreImage);
            Canvas.SetTop(semaphoreImage, top * size);
            Canvas.SetLeft(semaphoreImage, left * size);
        }

        public void updateBackPattern(Canvas canvas, int time)
        {
            int size = CommonData.Instance.getSizeRect();

            var w = canvas.ActualWidth;
            var h = canvas.ActualHeight;

            Uri roadUri = new Uri("pack://application:,,,/images/road.png", UriKind.Absolute);
            BitmapImage roadImg = new BitmapImage(roadUri);

            List<Point> points = Container.Instance.getPoints();
            List<ApplicationPetri.Semaphore> semafores = Container.Instance.getSemafores();

            for (int x = 0; x < w; x += size)
                for (int y = 0; y <= h; y += size)
                {
                    Image roadImage = new Image
                    {
                        Width = size,
                        Height = size,
                        Source = roadImg
                    };
                    canvas.Children.Add(roadImage);
                    Canvas.SetTop(roadImage, y);
                    Canvas.SetLeft(roadImage, x);

                    int newX = x / size;
                    int newY = y / size;

                    foreach (Point p in points)
                    {
                        if (p.getX() == newX && p.getY() == newY)
                        {
                            drawRect(canvas, p.getX(), p.getY(), p.getColor());
                        }
                    }

                    foreach (ApplicationPetri.Semaphore s in semafores)
                    {
                        if (s.getX() == newX && s.getY() == newY)
                        {
                             drawRectSemafore(canvas, s.getX(), s.getY(), 
                              CommonData.Instance.semaforeColor(time));
                        }
                    }
                }
            ExtensionMethods.Refresh(canvas);
        }

        public DateTime saveStatistic()
        {
            Statistic.Instance.clear();
            DateTime dt = DateTime.Now;
            String prevPath = System.Environment.GetFolderPath(
                Environment.SpecialFolder.CommonApplicationData);

            String path = "statistic_time" + dt.ToString();
            path=path.Replace(" ", "_").Replace(":", "_").Replace(".", "_");
            path = path + ".txt";
            path = Path.Combine(prevPath, path);
            
            using (TextWriter tw = new StreamWriter(path))
            {
                foreach (Int32 s in Statistic.Instance.getTimeList())
                {
                    tw.WriteLine(s.ToString());
                }
            }

            path = "statistic_transactions" + dt.ToString();
            path = path.Replace(" ", "_").Replace(":", "_").Replace(".", "_");
            path = path + ".txt";
            path = Path.Combine(prevPath, path);

            using (TextWriter tw = new StreamWriter(path))
            {
                foreach (Int32 s in Statistic.Instance.getTransactionsList())
                {
                    tw.WriteLine(s.ToString());
                }
            }

            path = "statistic_locks" + dt.ToString();
            path = path.Replace(" ", "_").Replace(":", "_").Replace(".", "_");
            path = path + ".txt";
            path = Path.Combine(prevPath, path);

            using (TextWriter tw = new StreamWriter(path))
            {
                foreach (Int32 s in Statistic.Instance.getMaxDeadlockLengthList())
                {
                    tw.WriteLine(s.ToString());
                }
            }
            return dt;
        }

        public void clear(Canvas canvas)
        {
            Container.Instance.clearObjects();
            Statistic.Instance.clear();
            updateBackPattern(canvas, 0);
        }

        private Task<bool> FuncAsync(int time)
        {
            return Task.Run(() =>
            {
                Container.Instance.run(time);
                return true;
            });
        }   

        public async void newThread(Canvas canvas)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            int x = CommonData.Instance.getX();
            Generator[] generators = new Generator[x]; 
            int[] nextGenerate = new int[x];

            for (int i = 0; i < x; i++)
            {
                generators[i] = new Generator(i,
                    CommonData.Instance.getInstensity());
                nextGenerate[i] = generators[i].generate() - 1;
                if (nextGenerate[i] < 0)
                {
                    Container.Instance.addObject(i, -1);
                }
            }
            

            for (int time = 0; time < CommonData.Instance.getTime(); time++)
            {
                
                for (int j = 0; j < x; j++)
                {
                    if (nextGenerate[j] == 0)
                    {
                        Container.Instance.addObject(j, -1);
                        nextGenerate[j] = generators[j].generate();
                    }
                }
                

                bool res = await FuncAsync(time);
                FormAdapter.Instance.updateBackPattern(canvas, time);
                Thread.Sleep(50);/////////////

                
                for (int j = 0; j < x; j++)
                {
                    if (nextGenerate[j] < 0)
                        nextGenerate[j] = generators[j].generate();
                    else
                        nextGenerate[j]--;
                }

                List<Point> pointList = new List<Point>();
                int ySize = CommonData.Instance.getY();

                foreach (Point p in Container.Instance.getPoints())
                {
                    if (p.getY() < ySize)
                        pointList.Add(p);
                }
                Container.Instance.setPoints(pointList);
            }
            Statistic.Instance.setDeadlockLength(Container.Instance.getDeadlockLength());
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            Statistic.Instance.setTime(ts.Seconds);

        }

    }
}
